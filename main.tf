terraform {
  backend "http" {
  }
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "3.1.0" 
    }
  }
}

resource "random_pet" "pet" {
  length    = 3
  separator = "-"
}

output "pet" {
  value = random_pet.pet.id
}



